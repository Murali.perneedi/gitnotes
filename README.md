## Available Scripts

In the project directory, you can run:

## Cd BackEnd

### `npm run start`

Runs backend server. Dependencies will be install automatically by this command

## Cd FrontEnd

### `npm run start`

Runs front end application. Dependencies will be install automatically by this command
