enum ALERT_MESSAGES {
  GistUpdated = 'Gist updated successfully!',
  GistCreated = 'Gist created successfully!',
  GistDeleted = 'Gist deleted successfully!',
  GistForked = 'Gist forked successfully!',
  GistStarred = 'Gist starred successfully!',
  GistUnstar = 'Gist Unstar successfully!',
  GistAdd = "Gist Add Successfully",
  CreateGistMissingFields = 'Please provide filename and content for gist',
}
export default ALERT_MESSAGES;
