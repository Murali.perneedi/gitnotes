export const CLIENT_ID = 'c3a3a7ae99945f112971';
export const REDIRECT_URL = `https://github.com/login/oauth/authorize?client_id=${CLIENT_ID}&scope=gist`;
export const GITHUB_PROFILE_BASE_URL = 'https://github.com/';
 