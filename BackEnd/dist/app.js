"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_consts_1 = require("./constants/common-consts");
var axios = __importStar(require("axios"));
var express_1 = __importDefault(require("express"));
var cors_1 = __importDefault(require("cors"));
var app = express_1.default();
app.use(cors_1.default());
// user authetication api to return access token
app.get('/authenticate-user', function (req, res) {
    var body = {
        client_id: req.query.client_id,
        client_secret: common_consts_1.CLIENT_SECRET,
        code: req.query.code,
    };
    var opts = { headers: { accept: 'application/json' } };
    axios.default
        .post(common_consts_1.ACCESS_TOKEN_REQUEST_URL, body, opts)
        .then(function (res) { return res.data['access_token']; })
        .then(function (_token) {
        console.log('Token:', _token, new Date().toISOString());
        res.send(_token);
    })
        .catch(function (err) { return res.status(500).json({ message: err.message }); });
});
app.use(express_1.default.static(__dirname + '/public'));
app.listen(5000, function () {
    console.log('Server listening on port : 5000');
});
